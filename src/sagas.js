import { put, takeEvery } from "redux-saga/effects";
import axios from "axios";

function* fetchData(action) {
  try {
    const { data } = yield axios.get(
      `https://api.github.com/search/repositories?q=${action.payload}`
    );
    yield put({ type: "FETCH_DATA_RECEIVED", payload: data.items });
  } catch (err) {
    yield put({ type: "FETCH_DATA_FAILD", payload: err });
  }
}

export function* rootSaga() {
  yield takeEvery("FETCH_DATA", fetchData);
}

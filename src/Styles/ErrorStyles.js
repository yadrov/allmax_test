import styled from 'styled-components'

export const ErrorSection = styled.section`
  margin: 0 auto;
`;
export const ErrorTitle = styled.h3`
  font-size:3rem;
  text-align:center;
`;
export const ErrorMessage = styled.p`
  text-align:center;
  font-size: 2rem;
`;
//@flow
import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { debounce } from "../util";
import List from "./List";
import Search from "./Search";
import Error from "./Error";

type AppProps = {
  projects: Array<Object>,
  errors: Array<Object>,
  FetchData: Function
};

class App extends Component<AppProps> {
  render() {
    return (
      <Fragment>
        <Search />
        {this.props.projects.length > 0 && (
          <List projects={this.props.projects} />
        )}
        {this.props.errors && <Error errors={this.props.errors} />}
      </Fragment>
    );
  }
}

export default connect(
  state => ({
    projects: state.projects,
    errors: state.errors
  }),
  dispatch => ({
    FetchData: debounce(value => {
      if (value.length < 3) return;
      dispatch({ type: "FETCH_DATA", payload: value });
    }, 700)
  })
)(App);

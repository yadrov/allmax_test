
//@flow
import React, { Fragment } from "react";
import { ErrorSection, ErrorTitle, ErrorMessage } from '../Styles/ErrorStyles'

type ErrorProps = {
  name: String,
  message:String
};


const Error = ({ errors } : { errors: ErrorProps }) => {
  const { name, message } = errors
    return (
      <ErrorSection>
        <ErrorTitle>{ name }</ErrorTitle>
        <ErrorMessage>{ message }</ErrorMessage>
      </ErrorSection>
    );
}

export default Error

//@flow
import React, { Component } from "react";
import { connect } from "react-redux";
import { debounce } from "../util";
import { SearchSection, InputSearch } from "../Styles/SearchStyle";

type SearchProps = {
  onFetchData: (text: string) => void
};

class Search extends Component<SearchProps> {
  render() {
    return (
      <SearchSection>
        <InputSearch
          type="text"
          placeholder="Enter search text"
          onChange={ event => this.props.onFetchData(event.target.value) }
        />
      </SearchSection>
    );
  }
}

export default connect(
  state => ({
  }),
  dispatch => ({
    onFetchData: debounce(value => {
      if (value.length < 3) return;
      dispatch({ type: "FETCH_DATA", payload: value });
    }, 700)
  })
)(Search);

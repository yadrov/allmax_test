import styled from "styled-components";

export const SearchSection = styled.div`
  width: 50%;
  margin: 6rem auto;
`;
export const InputSearch = styled.input`
  width: 100%;
  height: 1rem;
  font-size: 1.5rem;
  border: 1px solid lightgray;
  border-radius: 5px;
  padding: 1rem 1rem;
`;


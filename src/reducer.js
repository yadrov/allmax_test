
const initialState = {
  projects: [],
  errors: []
}
export default function reducer(state = initialState, action) {
  if (action.type === 'FETCH_DATA_RECEIVED') {
    if(action.payload.length === 0){
      return {
        ...state,
        errors: { name:'Nothing found' },
        projects: []
      }
    }
    return {
      ...state,
      projects: action.payload,
      errors: []
    }
  } else if (action.type === 'FETCH_DATA_FAILD') {
    return {
      ...state,
      errors: action.payload,
      projects: []
    }
  }

  return state
}



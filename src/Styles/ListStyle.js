import styled from 'styled-components'

export const ListSection = styled.section`
  margin: 0 auto;
`;
export const Table = styled.table`
  width: 80%;
  
`;

export const TBodyItem = styled.td`
  padding: 7px 0;
  border:2px solid lightgray;
  text-align: center; 
  font-size: 1.3rem;
`;
export const TBodyItemWithLink = styled(TBodyItem)`
  &:hover  {
      background-color: #ccc;
      color:lightgray
    }
`;

export const THeadItem = styled.th`
  font-size:1.3rem;
  padding: 7px 0;
`;
export const FirstColumnTHead = styled(THeadItem)`
  width:50%;
`;

export const TableLink = styled.a`
  text-decoration: none;
  display:block;
  padding: 5px;
  /* color:#000; */
`;
//@flow
import React from "react";

import {
  ListSection,
  Table,
  FirstColumnTHead,
  THeadItem,
  TBodyItem,
  TBodyItemWithLink,
  TableLink
} from "../Styles/ListStyle";

type ListProps = {
  projects: Array<Object>
};

const List = ({ projects }: { projects: ListProps }) => {
  return (
    <ListSection>
      <Table>
        <thead>
          <tr>
            <FirstColumnTHead>Repository</FirstColumnTHead>
            <THeadItem>Stars</THeadItem>
            <THeadItem>Watchers</THeadItem>
          </tr>
        </thead>
        <tbody>
          {projects.map(item => (
            <tr key={item.id}>
              <TBodyItemWithLink>
                <TableLink href={item.html_url}>{item.name}</TableLink>
              </TBodyItemWithLink>
              <TBodyItem>{item.watchers_count}</TBodyItem>
              <TBodyItem>{item.stargazers_count}</TBodyItem>
            </tr>
          ))}
        </tbody>
      </Table>
    </ListSection>
  );
};

export default List;
